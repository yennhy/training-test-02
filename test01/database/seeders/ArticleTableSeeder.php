<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Article;
use Faker\Generator as Faker;
use DB;

class ArticleTableSeeder extends Seeder
{
    public function __construct(Faker $faker)
    {
        $this->faker = $faker;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('article')->truncate();
        $data = [];
        for($i=1; $i<=50; $i++) {
            $data[] = [
                'title' => $this->faker->text($maxNbChars = 50),
                'content' => $this->faker->text($maxNbChars = 10000),
                'url_image' => $this->faker->imageUrl($width = 640, $height = 480, 'cats'),
                'view' => $this->faker->numberBetween($min = 0, $max = 2021),
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }
        Article::insert($data);
    }
}
