<nav class="navbar navbar-expand-lg navbar-dark bg-success">
    <div class="container justify-content-start">
        <a class="navbar-brand" href="/article"><i class="fas fa-home"></i></a>

        <div class="col-10">
            <form role="search" method="GET" id="formSearchTask" action="{{route('article.searchArticle')}}" enctype="multipart/form-data">
                <div class="input-group">
                    <input type="text" name="key" class="form-control" id="key_search" placeholder="Search Article">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit" id="btn_search">
                            <i class="fas fa-search text-white"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</nav>