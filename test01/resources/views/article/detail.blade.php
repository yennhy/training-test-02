@extends('layout.admin_layout')
@section('main_content')
    <div class="row mt-4">
        <div class="col-lg-8">
            <article>
                <header class="mb-4">
                    <h1 class="fw-bolder mb-1">{{ $articlesById->title }}</h1>
                    <div class="row mb-4">
                        <div class="col"> <i class="far fa-clock"></i> <span>{{ date('d/m/Y', strtotime($articlesById->created_at)) }}</span> </div>
                        <div class="col"> <i class="far fa-eye"></i> <span>{{ $articlesById->view }}</span> </div>
                        <div class="col"> <i class="far fa-heart tym"></i> <span id="count"></span></div>
                    </div>
                </header>
                <figure class="mb-4"><img class="img-fluid rounded" src="{{ $articlesById->url_image }}" alt="{{ $articlesById->title }}" /></figure>
                <section class="mb-5">
                    <p class="fs-5 mb-4">{{ $articlesById->content }}</p>
                </section>
            </article>
        </div>
        <div class="col-lg-4">
            @foreach ($articles as $key => $article)
                <div class="card mb-4">
                    <a href="{{ route('article.show', [$article->id]) }}"><img class="card-img-top height_200" src="{{ $article->url_image }}" alt="{{ $article->title }}" /></a>
                    <div class="card-body">
                        <div class="row">
                            <div class="col"> <i class="far fa-clock"></i> <span>{{ date('d/m/Y', strtotime($article->created_at)) }}</span> </div>
                            <div class="col"> <i class="far fa-eye"></i> <span>{{ $article->view }}</span> </div>
                        </div>
                        <h2 class="card-title h3">{{ $article->title }}</h2>
                        <p class="card-text">{{ $article->content }}</p>
                        <a class="btn btn-success" href="{{ route('article.show', [$article->id]) }}">Read more →</a>
                    </div>
                </div>
            @endforeach
            <div class="pagination justify-content-center">
                {{ $articles->links('pagination::bootstrap-4') }}
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ mix('js/likeArticle.js') }}"></script>
@endsection
