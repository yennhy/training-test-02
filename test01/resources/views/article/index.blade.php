@extends('layout.admin_layout')
@section('main_content')
    <div class="row mt-4">
        <h1 class="fw-bolder mb-4 text-center">Danh sách bài viết</h1>
        <div class="col-lg-8">
            @foreach ($articles as $key => $article)
                @if ($key==0)
                    <div class="card mb-4">
                        <a href="{{ route('article.show', [$article->id]) }}"><img class="card-img-top" src="{{ $article->url_image }}" alt="{{ $article->title }}" /></a>
                        <div class="card-body">
                            <div class="row mb-4">
                                <div class="col"> <i class="far fa-clock"></i> <span>{{ date('d/m/Y', strtotime($article->created_at)) }}</span> </div>
                                <div class="col"> <i class="far fa-eye"></i> <span>{{ $article->view }}</span> </div>
                            </div>
                            <h2 class="card-title">{{ $article->title }}</h2>
                            <p class="card-text">{{ $article->content }}</p>
                            <a class="btn btn-success" href="{{ route('article.show', [$article->id]) }}">Read more →</a>
                        </div>
                    </div>
                @endif

                @if ($key%2==0 & $key!=0 & $key!=1 & $key!=2)
                    <div class="row mt-4 mb-4">
                        <div class="col">
                            <a href="{{ route('article.show', [$article->id]) }}"><img class="card-img-top height_200" src="{{ $article->url_image }}" alt="{{ $article->title }}" /></a>
                        </div>
                        <div class="col">
                            <h2 class="card-title h3">{{ $article->title }}</h2>
                            <div class="row mb-4">
                                <div class="col"> <i class="far fa-clock"></i> <span>{{ date('d/m/Y', strtotime($article->created_at)) }}</span> </div>
                                <div class="col"> <i class="far fa-eye"></i> <span>{{ $article->view }}</span> </div>
                            </div>
                            <p class="card-text">{{ $article->content }}</p>
                            <a class="btn btn-success" href="{{ route('article.show', [$article->id]) }}">Read more →</a>
                        </div>
                    </div>
                    <hr class="my-0" />
                @endif

                @if ($key%2!=0 & $key!=0 & $key!=1 & $key!=2)
                    <div class="row mt-4 mb-4">
                        <div class="col">
                            <h2 class="card-title h3">{{ $article->title }}</h2>
                            <div class="row mb-4">
                                <div class="col"> <i class="far fa-clock"></i> <span>{{ date('d/m/Y', strtotime($article->created_at)) }}</span> </div>
                                <div class="col"> <i class="far fa-eye"></i> <span>{{ $article->view }}</span> </div>
                            </div>
                            <p class="card-text">{{ $article->content }}</p>
                            <a class="btn btn-success" href="{{ route('article.show', [$article->id]) }}">Read more →</a>
                        </div>
                        <div class="col">
                            <a href="{{ route('article.show', [$article->id]) }}"><img class="card-img-top height_200" src="{{ $article->url_image }}" alt="{{ $article->title }}" /></a>
                        </div>
                    </div>
                    <hr class="my-0" />
                @endif
            @endforeach
            
            <div class="pagination mt-4 justify-content-center">
                {{ $articles->links('pagination::bootstrap-4') }}
            </div>

            <div class="text-right">
                <div class="d-inline-block"></div>
            </div>
        </div>
        <div class="col-lg-4">
            @foreach ($articles as $key => $article)
                @if ($key==1)
                    <div class="card mb-4">
                        <a href="{{ route('article.show', [$article->id]) }}"><img class="card-img-top height_200" src="{{ $article->url_image }}" alt="{{ $article->title }}" /></a>
                        <div class="card-body">
                            <div class="row mb-4">
                                <div class="col"> <i class="far fa-clock"></i> <span>{{ date('d/m/Y', strtotime($article->created_at)) }}</span> </div>
                                <div class="col"> <i class="far fa-eye"></i> <span>{{ $article->view }}</span> </div>
                            </div>
                            <h2 class="card-title h3">{{ $article->title }}</h2>
                            <p class="card-text">{{ $article->content }}</p>
                            <a class="btn btn-success" href="{{ route('article.show', [$article->id]) }}">Read more →</a>
                        </div>
                    </div>
                @endif

                @if ($key==2)
                    <div class="card mb-4">
                        <a href="{{ route('article.show', [$article->id]) }}"><img class="card-img-top height_200" src="{{ $article->url_image }}" alt="{{ $article->title }}" /></a>
                        <div class="card-body">
                            <div class="row mb-4">
                                <div class="col"> <i class="far fa-clock"></i> <span>{{ date('d/m/Y', strtotime($article->created_at)) }}</span> </div>
                                <div class="col"> <i class="far fa-eye"></i> <span>{{ $article->view }}</span> </div>
                            </div>
                            <h2 class="card-title h3">{{ $article->title }}</h2>
                            <p class="card-text">{{ $article->content }}</p>
                            <a class="btn btn-success" href="{{ route('article.show', [$article->id]) }}">Read more →</a>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
@endsection
