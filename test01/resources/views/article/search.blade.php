@extends('layout.admin_layout')
@section('main_content')
    {{-- <div class="container"></div> --}}
        <div class="row mt-4 height_800">
            <h4 class="mt-4"> {{ $search->total() }} Kết quả tìm kiếm cho: " {{ $keySearch }}" </h4>

            @foreach ($search as $key => $value)
                <div class="row mt-4 mb-4">
                    <div class="col">
                        <h2 class="card-title h3">{{ $value->title }}</h2>
                        <div class="row mb-4">
                            <div class="col"> <i class="far fa-clock"></i> <span>{{ date('d/m/Y', strtotime($value->created_at)) }}</span> </div>
                            <div class="col"> <i class="far fa-eye"></i> <span>{{ $value->view }}</span> </div>
                        </div>
                        <p class="card-text">{{ $value->content }}</p>
                        <a class="btn btn-success" href="{{ route('article.show', [$value->id]) }}">Read more →</a>
                    </div>
                    <div class="col">
                        <a href="{{ route('article.show', [$value->id]) }}"><img class="card-img-top height_200" src="{{ $value->url_image }}" alt="{{ $value->title }}" /></a>
                    </div>
                </div>
                <hr class="my-0" />
            @endforeach
            <div class="pagination mt-4 justify-content-center">
                {{ $search->appends(['key' => $keySearch])->links('pagination::bootstrap-4') }}
            </div>
        </div>
@endsection
