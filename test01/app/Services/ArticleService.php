<?php
namespace App\Services;

use App\Services\Contracts\ArticleServiceContract;
use App\Repositories\Contracts\ArticleRepoContract as ArticleRepository;

class ArticleService implements ArticleServiceContract 
{
    public function __construct( ArticleRepository $articleRepository ) {
        $this->articleRepository = $articleRepository;
    }

    public function getAllArticles($input = [])
    {
        $input['limit'] = config('article.list_article_condition.limit');
        $articles = $this->articleRepository->getAllArticles($input);
        return $articles;
    }

    public function updateView($id)
    {
        $view = $this->articleRepository->updateView($id);
    }

    public function getArticleById($id)
    {
        $articles = $this->articleRepository->getArticleById($id);
        return $articles;
    }

    public function getSearch($input = [])
    {
        $input['limit'] = config('article.list_article_condition.limit');
        $input['order_by'] = config('article.list_article_condition.order_by');
        $input['order_type'] = config('article.list_article_condition.order_type');
        $search = $this->articleRepository->getSearch($input);
        return $search;
    }

}