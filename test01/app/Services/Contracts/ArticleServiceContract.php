<?php
namespace App\Services\Contracts;

interface ArticleServiceContract 
{
    public function getAllArticles($input = []);

    public function updateView($id);

    public function getArticleById($id);

    public function getSearch($input = []);

}
