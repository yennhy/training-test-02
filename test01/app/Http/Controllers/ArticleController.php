<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Contracts\ArticleServiceContract as ArticleService;

class ArticleController extends Controller
{
    public function __construct(ArticleService $articleService) {
        $this->articleService = $articleService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = $this->articleService->getAllArticles();
        return view('article.index', ['articles' => $articles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $updateView = $this->articleService->updateView($id);
        $articlesById = $this->articleService->getArticleById($id);
        $articles = $this->articleService->getAllArticles();
        return view('article.detail', [
            'articlesById' => $articlesById,
            'articles' => $articles,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function searchArticle(Request $request)
    {
        $input = $request->all();
        $keySearch = $input['key'];
        if (!is_null($input['key'])) {
            $search = $this->articleService->getSearch($input);
        } else {
            return redirect()->route('article.index');
        }
        return view('article.search', [
            'search' => $search,
            'keySearch' => $keySearch,
        ]);
    }
}
