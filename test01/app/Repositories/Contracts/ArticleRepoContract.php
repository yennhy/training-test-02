<?php
namespace App\Repositories\Contracts;

interface ArticleRepoContract 
{
    public function getAllArticles($input = []);

    public function updateView($id);

    public function getArticleById($id);
    
    public function getSearch($input = []);

}