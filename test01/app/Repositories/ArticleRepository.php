<?php
namespace App\Repositories;

use App\Models\Article;
use App\Repositories\Contracts\ArticleRepoContract;
use Illuminate\Support\Facades\DB;

class ArticleRepository implements ArticleRepoContract 
{
    public function __construct(Article $article)
    {
        $this->model = $article;
    }

    public function getAllArticles($input = [])
    {
        $query = $this->model;
        $result = $query->paginate($input['limit']); 
        return $result;
    }

    public function updateView($id)
    {
        $query = $this->model;
        $query = $query->select('view');
        $query = $query->where('id', $id);
        $result = $query->first();
        $view = $result['view'];

        $query2 = $this->model;
        $query2 = $query2->where('id', $id);
        $query2 = $query2->update(['view' => $view + 1]);
    }

    public function getArticleById($id)
    {
        $query = $this->model;
        $query = $query->where('id', $id);
        $result = $query->first();
        return $result;
    }

    public function getSearch($input = [])
    {
        $query = $this->model;
        $query = $query->where('title', 'like', '%'.$input['key'].'%');
        $query = $query->orderBy($input['order_by'], $input['order_type']);
        $result = $query->paginate($input['limit']);
        return $result;
    }
}